$(window).on('load', function () {
    $('#loading').fadeOut(5, function(){
      $('#main_div').show();
    });
  });

  var favorit=0;
  var onGBook = false;

  $(document).ready(function(){
    callAjax("get-books2/");
    setCounter(0);
});


function setCounter(cnt){
    $("#counter").html(cnt);
}

function createInformationBooks(title,authors,description) {
    var res="";
    res+="<div class='wrapper_text_inside'>";
    res+="<h3>"+title+"</h3>";
    res+="<p><b>"+authors+"</b>";
    res+="<br>"+description+"</p>";
    res+="</div>"
    return res;
}

function createFav(id) {
    return "<i id='star-"+id+"' class='far fa-star' onClick='FavMe(`"+id+"`)' style='font-size:45px'></i>";
 }

 function FavMe(id) {
    id = id+""
    if("rgb(33, 37, 41)" === $("#star-"+id).css('color') ){
        $("#star-"+id).css('color','rgb(249, 214, 34)');
        favorit++;
        setCounter(favorit);
    } else if("rgb(249, 214, 34)" === $("#star-"+id).css('color') ){
        $("#star-"+id).css('color','rgb(33, 37, 41)');
        favorit--;
        setCounter(favorit);
    }

  }

$('#btnStory').click(function(){
    console.log("click:btnStory");
    callAjax("get-books/");
    onGBook = true;
})

$('#btnChal1').click(function(){
    console.log("click:btnChal1");
    callAjax("get-books2/");
    onGBook = true;
})

$('#btnChal2').click(function(){
    console.log("click:btnChal2");
    callAjax("get-books3/");
    onGBook = true;
})

function setDataTable() {
    console.log("DataTable");
    $('#table-result').DataTable();
 }


function callAjax(param) {
    if(param === "get-books/" & onGBook){
        return;
    }
    if(param === "get-books2/" & onGBook){
        return;
    }
    if(param === "get-books3/" & onGBook){
        return;
    }
    $.ajax({
        url:"/"+param,
        dataType:"json",
        success: function (response) {
            console.log("success");
            var lists = response["data"];
            var append = "";
            for (i = 0; i < lists.length; i++){
                append+="<tr>";
                append+="<td>"+(i+1)+"</td>";
                append+="<td>"+lists[i]['title']+"</td>";//kolom T A D
                append+="<td>"+lists[i]['authors']+"</td>";//kolom T A D
                append+="<td style='text-align:justify'>"+lists[i]['description']+"</td>";//kolom T A D
                append+="<td>"+"<img class='cover-image' src="+lists[i]['cover']+">"+"</td>";//kolom cover
                append+="<td>"+createFav(lists[i]['id'])+"</td>";//kolom kolom favorite
                append+="<tr>";
            }
            $('#result').html(append);
        }

    })
 }
