from django.test import TestCase, Client, LiveServerTestCase
from datetime import datetime
from django.urls import resolve, reverse
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class appMALunitTest(TestCase):

    def test_view(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

