"""PPW URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import re_path,path
from django.conf.urls import url
from .views import *

urlpatterns = [
    path('get-books/',get_Google_book,name='get-google-books'),
    path('get-books2/',get_Google_book2,name='get-google-books2'),
    path('get-books3/',get_Google_book3,name='get-google-books3'),
    path('', view_books),
]   
