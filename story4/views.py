from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse,JsonResponse
import requests
import json

response = {}
# Create your views here.
def view_books(request):
    return render(request,"books.html")

def get_Google_book(request):
    print("--> GOOGLE_BOOK")
    raw_data = requests.get("https://www.googleapis.com/books/v1/volumes?q=coloring").json()

    data = [];
    for info in raw_data['items'] :
        item = {}
        item['id'] = info['id']
        item['title'] = info['volumeInfo']['title']
        item['authors'] = ", ".join(info['volumeInfo']['authors'])
        item['description'] = info['volumeInfo']['description'][:120]+"..."
        item['cover']=info['volumeInfo']['imageLinks']['thumbnail']
        data.append(item)

    return JsonResponse({"data" : data})


def get_Google_book2(request):
    print("--> GOOGLE_BOOK2")
    raw_data = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting").json()

    data = [];
    for info in raw_data['items'] :
        item = {}
        item['id'] = info['id']
        item['title'] = info['volumeInfo']['title']
        item['authors'] = ", ".join(info['volumeInfo']['authors'])
        item['description'] = info['volumeInfo']['description'][:120]+"..."
        item['cover']=info['volumeInfo']['imageLinks']['thumbnail']
        data.append(item)

    return JsonResponse({"data" : data})

def get_Google_book3(request):
    print("--> GOOGLE_BOOK3")
    raw_data = requests.get("https://www.googleapis.com/books/v1/volumes?q=gambling").json()

    data = [];
    for info in raw_data['items'] :
        item = {}
        item['id'] = info['id']
        item['title'] = info['volumeInfo']['title']
        item['authors'] = ", ".join(info['volumeInfo']['authors'])
        item['description'] = info['volumeInfo']['description'][:120]+"..."
        item['cover']=info['volumeInfo']['imageLinks']['thumbnail']
        data.append(item)

    return JsonResponse({"data" : data})